
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
//#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <EEPROM.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>


/* Just a little test message.  Go to http://192.168.4.1 in a web browser */


 
// ---------------------------------------------------
//                      DEFINE
// ---------------------------------------------------
#define M1_FOR_PWM D1 // motor 1 forward pwm
#define M1_REV_PWM D2 // motor 1 reverse pwm
#define M2_FOR_PWM D3 // motor 2 forward pwm
#define M2_REV_PWM D4 // motor 2 reverse pwm

#define BL_R D7       // backlight red color
#define BL_G D6       // backlight green color
#define BL_B D5       // backlight blue color



// ---------------------------------------------------
//                     VARIABLES
// ---------------------------------------------------
ADC_MODE(ADC_VCC);

String robotName = String("AMUR-robot-") + String(ESP.getChipId());

const char *selfSsid = robotName.c_str();
const char *selfPassword = "q1q2"; 

String descriptionModeSelfAP = "AP Did't find. Self AP was enabled (" + String(selfSsid) + ")";
String descriptionModeExtrAP = "AP was found. Connected to it.";

volatile boolean flagFindAP = false;

String ssid = "";
String pass = "";

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
AsyncEventSource events("/events");

String mainPage = "";



// ---------------------------------------------------
//                CLIENT-SIDE: JAVASCRIPT
// ---------------------------------------------------
const char HTML_SCRIPT_TAG[] PROGMEM = R"(
    <script type="text/javascript">
      var flagKeyEvent=false;
      var HttpClient = function() {
            this.get = function(aUrl, aCallback) {
            var anHttpRequest = new XMLHttpRequest();
            anHttpRequest.onreadystatechange = function() { 
                    if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                      aCallback(anHttpRequest.responseText);
            }

            anHttpRequest.open( "GET", aUrl, true );            
            anHttpRequest.send( null );
          }
      }
      var xmlHttp = new XMLHttpRequest();
      var client = new HttpClient();
      function moveForward(){
        let mspeed = document.getElementById("motionspeed").value;
        client.get('http://' + window.location.host + '/wheel/left?speed=' + mspeed, function(response) {});
        client.get('http://' + window.location.host + '/wheel/right?speed=' + mspeed, function(response) {});
      }
      function moveBackward(){
        let mspeed = document.getElementById("motionspeed").value;
        client.get('http://' + window.location.host + '/wheel/left?speed=-' + mspeed, function(response) {});
        client.get('http://' + window.location.host + '/wheel/right?speed=-' + mspeed, function(response) {});
      }
      function rotateLeft(){
        let rspeed = document.getElementById("rotatespeed").value;
        client.get('http://' + window.location.host + '/wheel/left?speed=-' + rspeed, function(response) {});
        client.get('http://' + window.location.host + '/wheel/right?speed=' + rspeed, function(response) {});
        
      }
      function rotateRight(){
        let rspeed = document.getElementById("rotatespeed").value;
        client.get('http://' + window.location.host + '/wheel/left?speed=' + rspeed, function(response) {});
        client.get('http://' + window.location.host + '/wheel/right?speed=-' + rspeed, function(response) {});
      }
      function stopWheels(){
        client.get('http://' + window.location.host + '/wheel/left?speed=1', function(response) {});
        client.get('http://' + window.location.host + '/wheel/right?speed=1', function(response) {});
      }
      function initKeyEvents(){
        document.getElementById("keyinput").addEventListener('keydown', (event) => {
        if (event.defaultPrevented || flagKeyEvent) {
          return; 
        }
        flagKeyEvent=true;
        switch (event.key) {
          case "ArrowDown":
             moveBackward();
             break;
          case "ArrowUp":
              moveForward();
              break;
          case "ArrowLeft":
              rotateLeft();
              break;
          case "ArrowRight":
               rotateRight();
              break;
          default:
              return; // Quit when this doesn't handle the key event.
        }
        event.preventDefault();
      }, false);
      document.getElementById("keyinput").addEventListener('keyup', (event) => {
        if (event.defaultPrevented) {
          return; 
        }
        flagKeyEvent=false;
        switch (event.key) {
          case "ArrowDown":
             stopWheels();
             break;
          case "ArrowUp":
              stopWheels();
              break;
          case "ArrowLeft":
              stopWheels();
              break;
          case "ArrowRight":
               stopWheels();
              break;
          default:
              return; // Quit when this doesn't handle the key event.
        }
        event.preventDefault();
      }, false); 
      }
      document.addEventListener('DOMContentLoaded', function(e) { initKeyEvents();}, true);
    </script>
    )";



// ---------------------------------------------------
//                    UTIL FUNCTIONS
// ---------------------------------------------------
int clamp(int value, int min, int max){
  if(value > max) return max;
  if(value < min) return min;
  return value;
}

int clampSpeed(int value){
  return clamp(value, 0, 512);
}

boolean isValidNumber(String str){
  for(int i=0; i < str.length(); i+=1){
    if(!isDigit(str[i]) && !str[i]=='-' && !str[i] == '.'){
      return false;
    }
    return true;
  }
}

void readAPConfig(){
  for (int i = 0; i < 32; ++i){
      ssid += char(EEPROM.read(i));
  }
  for (int i = 32; i < 96; ++i){
      pass += char(EEPROM.read(i));
  }
  if(ssid.length() == 0){
    ssid = "junk";
  }
  if(pass.length() == 0){
    pass = "junk";
  }
}

bool testWifi(void) {
  int c = 0;
  while ( c < 4 ) {
    if (WiFi.status() == WL_CONNECTED) { return true; } 
    delay(500);
    Serial.print(WiFi.status());    
    c++;
  }
  return false;
}



// ---------------------------------------------------
//                    HANDLE: WEB_REQUEST
// ---------------------------------------------------
void handleRoot(AsyncWebServerRequest *request) {
  request->send(200, "text/html", mainPage);
}

void handleWheel1(AsyncWebServerRequest *request){
  String speed = request->arg("speed");
  if(!isValidNumber(speed)){
    request->send(404, "application/json", R"({"state":"error", "description": "not valid param"})");
    return;
  }
  int intSpeed = speed.toInt();
  if(intSpeed < 0 || intSpeed == 0){
    int resSpeed = clampSpeed(abs(intSpeed)) * 2;
    analogWrite(M1_FOR_PWM, 0);
    analogWrite(M1_REV_PWM, resSpeed);
    request->send(200, "text/json", R"({"state":"ok", "description": ""})");
  }
  if(intSpeed > 0){
    int resSpeed = clampSpeed(abs(intSpeed)) * 2;
    analogWrite(M1_FOR_PWM, resSpeed);
    analogWrite(M1_REV_PWM, 0);
    request->send(200, "text/json", R"({"state":"ok", "description": ""})");
  }
  
}

void handleWheel2(AsyncWebServerRequest *request){
  String speed = request->arg("speed");
  if(!isValidNumber(speed)){
    request->send( 404, "application/json", R"({"state":"error", "description": "not valid param"})");
    return;
  }
  int intSpeed = speed.toInt();
  if(intSpeed < 0 || intSpeed == 0){
    int resSpeed = clampSpeed(abs(intSpeed)) * 2;
    analogWrite(M2_FOR_PWM, 0);
    analogWrite(M2_REV_PWM, resSpeed);
    request->send(200, "text/json", R"({"state":"ok", "description": ""})");
  }
  if(intSpeed > 0){
    int resSpeed = clampSpeed(abs(intSpeed)) * 2;
    analogWrite(M2_FOR_PWM, resSpeed);
    analogWrite(M2_REV_PWM, 0);
    request->send(200, "text/json", R"({"state":"ok", "description": ""})");
  }
}

void backlight(AsyncWebServerRequest *request){
  String r = request->arg("r");
  String g = request->arg("g");
  String b = request->arg("b");
  if(!isValidNumber(r) || !isValidNumber(g) || !isValidNumber(b)){
    request->send( 404, "application/json", R"({"state":"error", "description": "not valid param"})");
    return;
  }
  int intR = clamp(r.toInt(), 0, 1024);
  int intG = clamp(g.toInt(), 0, 1024);
  int intB = clamp(b.toInt(), 0, 1024);
  analogWrite(BL_R, intR);
  analogWrite(BL_G, intG);
  analogWrite(BL_B, intB);
  request->send(200, "text/json", R"({"state":"ok", "description": ""})");
}

void setAP(AsyncWebServerRequest *request){
  String qsid = request->arg("ssid");
  String qpass = request->arg("pass");
  if (qsid.length() > 0 && qpass.length() > 0) {
    for (int i = 0; i < 96; ++i) {
      EEPROM.write(i, 0); 
    } 
    for (int i = 0; i < qsid.length(); ++i){
      EEPROM.write(i, qsid[i]);
    }
    for (int i = 0; i < qpass.length(); ++i){
      EEPROM.write(32+i, qpass[i]);
    }    
    EEPROM.commit();
    request->send(200, "application/json", R"({"state":"ok", "description": "saved to eeprom... reset to boot into new wifi"})");
  }
  request->send( 404, "application/json", R"({"state":"error", "description": "not valid param"})");
}

void handleLed(AsyncWebServerRequest *request){
  String state = request->arg("state");
  if(state == "on"){
    digitalWrite(LED_BUILTIN, LOW);
    request->send(200, "application/json", R"({"state":"ok", "description": ""})");
    return;
  }
  if(state == "off"){
    digitalWrite(LED_BUILTIN, HIGH);
    request->send(200, "application/json", R"({"state":"ok", "description": ""})");
    return;
  }
  request->send(400, "application/json", R"({"state":"error", "description": "not valid param"})");
}



// ---------------------------------------------------
//                CLIENT-SIDE: HTML
// ---------------------------------------------------
void initMainPage(){
  mainPage = "";
  mainPage += String("") + 
    "<style type=\"text/css\">" + 
    "th,td{align-items: left;}" + 
    "</style>" + 
    FPSTR(HTML_SCRIPT_TAG) + 
    "<h1>" + robotName + " robot page</h1>" +    
    "<h2> General information: </h2>" + 
    "<p> <b>Local IP:</b> " + WiFi.localIP().toString() +  "</p>" +  
    "<p> <b>WiFi Mode:</b> " + String(flagFindAP == false ? descriptionModeSelfAP : descriptionModeExtrAP) + "</p>" +
    "<p> <b>Configured AP SSID:</b> " + ssid.c_str() + "</p>"  +
    "<p> <b>Configured AP Pass:</b> ********* </p>" +

    "<h2> Manual control: </h2>" + 
    "<table>" + 
    "<tr> <td></td> <td><button onmousedown=\"moveForward()\" onmouseup=\"stopWheels()\">&uArr;</button></td> <td></td> </tr>" + 
    "<tr>" + 
      "<td><button onmousedown=\"rotateLeft()\" onmouseup=\"stopWheels()\">&lArr;</button></td>" + 
      "<td><button onmousedown=\"moveBackward()\" onmouseup=\"stopWheels()\">&dArr;</button></td>" + 
      "<td><button onmousedown=\"rotateRight()\" onmouseup=\"stopWheels()\">&rArr;</button></td>" + 
    "</tr>" + 
    "</table>" + 
    
    R"(<div> <b>Motion speed:</b>  <input class="slider" type="range" min="0" max="511" value="200" id="motionspeed"> </div>)" +
    R"(<div> <b>Rotate speed :</b> <input class="slider" type="range" min="0" max="511" value="200" id="rotatespeed"> </div>)" + 
    R"(<div> <b>Keyboard control: </b> <input id="keyinput"> </div>)" + 
    
    "<h2> API: </h2>" +  
    "<table border=\"1\">" + 
    "<tr><th>Action</th><th>Method</th><th>URL</th><th>Params</th><th>Return</th></tr>" + 
    "<tr><td>Set access point</td><td>GET</td><td>/set/ap</td><td>ssid=string&pass=string</td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "<tr><td>Restart ESP</td><td>GET</td><td>/restart</td><td></td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "<tr><td>ESP led</td><td>GET</td><td>/led</td><td>state=on|off</td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "<tr><td>Left wheel</td><td>GET</td><td>/wheel/left</td><td>speed=-512..512</td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "<tr><td>Right wheel</td><td>GET</td><td>/wheel/right</td><td>speed=-512..512</td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "<tr><td>Backlight</td><td>GET</td><td>/backlight</td><td>r=0..1024&g=0..1024&b=0..1024</td><td>{\"state\":error|ok, \"description\":string}</td></tr>" + 
    "</table>" + 
    
    "<h2> Hardware information </h2>" + 
    "<p> <b>Chip ID:</b> " + ESP.getChipId() + "</p>" + 
    "<p> <b>CPU frequency:</b> " + ESP.getCpuFreqMHz() + " MHz </p>" + 
    "<p> <b>Sketch MD5:</b> " + ESP.getSketchMD5() + "</p>" + 
    "<p> <b>Board VCC:</b> " + ESP.getVcc() + "</p>" + 
    "<p> <b>Last restart reason:</b> " + ESP.getResetReason() + "</p>";   
}



// ---------------------------------------------------
//                    MAIN: SETUP
// ---------------------------------------------------
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  pinMode(M1_REV_PWM, OUTPUT);
  pinMode(M1_FOR_PWM, OUTPUT);
  pinMode(M2_REV_PWM, OUTPUT);
  pinMode(M2_FOR_PWM, OUTPUT);
  pinMode(BL_R, OUTPUT);
  pinMode(BL_G, OUTPUT);
  pinMode(BL_B, OUTPUT);
  
  EEPROM.begin(512);
  delay(10);
  readAPConfig();
  
	delay(1000);
	Serial.begin(115200);
  //Serial.setDebugOutput(true);
  WiFi.begin(ssid.c_str(), pass.c_str());
  flagFindAP = testWifi();
  if(flagFindAP){
    // print 
  }else{
    // print local ip
  }
  initMainPage();
	WiFi.softAP(selfSsid);
  MDNS.begin(selfSsid);
 
	server.on("/", handleRoot);
  server.on("/wheel/left",  HTTP_GET, handleWheel1); 
  server.on("/wheel/right", HTTP_GET, handleWheel2); 
  server.on("/led", HTTP_GET, handleLed);
  server.on("/set/ap", HTTP_GET, setAP); 
  server.on("/backlight", HTTP_GET, backlight); 
  server.on("/restart", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "application/json", R"({"status":"restaring"})");
    ESP.restart();
  });
 
  server.begin();
  MDNS.addService("http","tcp",80);
  
  delay(1000);
}



// ---------------------------------------------------
//                    MAIN: LOOP
// ---------------------------------------------------
void loop() {
}




